<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once('modelo.php');
require_once('vistas.php');

class MainController {    

    function handleAction($action) {
        $vistas = new Vistas();
        $orm = new TwitsORM();
        $matches = array();

        if ($action == "/") {            
            echo $vistas->renderIndex();

        } else if (preg_match('#/tweets/(?P<user>\w+)#', $action, $matches)) {
            $user = $matches["user"];
            echo $vistas->renderTwitsFromUser($user, $orm->getTwitsFromUser($user));

        } if (preg_match('#/tweets/?#', $action)) {
            echo $vistas->renderAllTwits($orm->getTwits());

        } else {
            echo "ERROR: Not found";
            http_response_code(404); // PHP >= 5.4 only
        }
    }
}

?>


