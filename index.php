<?php

error_reporting(-1);
ini_set('display_errors', 'On');

include('controller.php');

$baseURL = "http://afrodita.inf.ucv.cl/~ifigueroa/WebServices";
$requestURL = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$requestString = substr($requestURL, strlen($baseURL));
$urlParams = array_slice(explode('/', $requestString), 2);
$action = '/' . implode('/', $urlParams);

/*
echo $requestURL;
echo "<br/>";
var_dump($urlParams);
echo $action;
*/

$controller = new MainController();
$controller->handleAction($action);

?>