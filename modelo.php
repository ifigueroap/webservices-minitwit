<?php

error_reporting(-1);
ini_set('display_errors', 'On');

class TwitsORM {
    
    public static function getTwitsFromUser($user) {        
        $db = new SQLite3("minitwits.db");
        $results = $db->query("SELECT * FROM twits WHERE twit_user = '{$user}'");
        $dataset = array();

        while($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $dataset[] = $row;
        }

        return $dataset;
    }

    public static function getTwits() {        
        $db = new SQLite3("minitwits.db");
        $results = $db->query("SELECT * FROM twits");
        $dataset = array();

        while($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $dataset[] = $row;
        }

        return $dataset;
    }
}

?>