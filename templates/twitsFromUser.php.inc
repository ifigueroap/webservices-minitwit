<html>
  <head>
    <title><?= $context["title"] ?></title>
  </head>
  <body>
    <h1>Twits de <?= $context["user"] ?></h1>

    <ul>
    <?php foreach($context["userTwits"] as $twit) { ?>
      <li><?= $twit["twit_msg"] ?></li>
    <?php } ?>
    </ul>
  </body>
</html>
