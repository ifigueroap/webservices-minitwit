<?php

error_reporting(-1);
ini_set('display_errors', 'On');

class Vistas {

    function renderTemplate($template, $context) {
        ob_start();

        include "templates/{$template}.php.inc";

        $page = ob_get_contents();
        ob_end_clean();
        return $page;
    }


    // ====================================================
    function renderIndex() {       

        return $this->renderTemplate(
            "index"
            , array("title" => "Hola Mundo Twit"
                    , "foobar" => "Esta es la pagina principal")
        );
    }

    // ====================================================
    function renderTwitsFromUser($user, $userTwits) {
        $context["user"] = $user;
        $context["userTwits"] = $userTwits;

        return $this->renderTemplate(
            "twitsFromUser"
            , array("user" => $user
                    , "userTwits" => $userTwits)
        );
    }

    // ====================================================
    function renderAllTwits($allTwits) {
        return $this->renderTemplate(
            "allTwits"
            , array("allTwits" => $allTwits)
        );
    }
}