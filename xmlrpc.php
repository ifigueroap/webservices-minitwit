<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once('modelo.php');


$orm = new TwitsORM();

function getTwitsFrom($method, $args) {
    global $orm;
    return $orm->getTwitsFromUser($args[0]);
}

function getTwits($method, $args) {
    global $orm;
    return $orm->getTwits();
}

$request = file_get_contents('php://input');
$server = xmlrpc_server_create( );
if (!$server) die("Couldn't create server");

xmlrpc_server_register_method($server, 'twits', 'getTwits');
xmlrpc_server_register_method($server, 'twitsFrom', 'getTwitsFrom');

$options = array('output_type' => 'xml', 'version' => 'auto');
echo xmlrpc_server_call_method($server, $request, null, $options);

xmlrpc_server_destroy($server);

?>


